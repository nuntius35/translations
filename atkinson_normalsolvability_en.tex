\documentclass[12pt]{article}

\usepackage[a4paper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage[english]{babel}
\usepackage[final,
            activate={true,nocompatibility},
            babel,
            kerning=true,
            spacing=true,
            factor=1700,
            stretch=20,
            shrink=50]
            {microtype}
\usepackage[all]{nowidow}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{bookmark}
\usepackage{enumitem}

\newtheorem{thm}{Theorem}
\newtheorem*{lem}{Lemma}

\newcommand{\abs}[1]{\left\lvert{#1}\right\rvert}
\newcommand{\norm}[1]{\left\lVert{#1}\right\rVert}

\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}
\renewcommand{\thesection}{\S~\arabic{section}.}
\renewcommand{\thethm}{\Roman{thm}}
\pagestyle{fancy}

\hypersetup{
  pdfinfo={
      Title={Normal solvability of linear equations in normed spaces},
      Author={F.~V.~Atkinson},
      Subject={Translated from the Russian original by Andreas Geyer-Schulz}
  }
}

\begin{document}
\fancyhead{1951 \hfill Matematicheskii Sbornik \hfill T.~28 (70), No.~1}

\title{Normal solvability of linear equations in normed spaces}
\author{F. V. Atkinson (Ibadan, Nigeria)}
\date{}
\maketitle

\section{\texorpdfstring{}{§ 1}}
Here we are studying solutions of the equation
\begin{equation} \label{eq11}
  Tf = g,
\end{equation}
where $f$ and $g$ are elements of the complex Banach space $R$ and $T$ is a bounded linear operator on $R$.

We shall denote by $A(T)$ and $B(T)$ the following linear subspaces:
$A(T)$ is the linear subspace of $R$, wich consists of all elements $\varphi$ for which
\begin{equation*}
  T\varphi = 0,
\end{equation*}
and $B(T)$ is the linear subspace of the dual space $\overline{R}$, consisting of all bounded linear functionals $l$, for which
\begin{equation*}
  lT = 0.
\end{equation*}
In our case $A(T)$ and $B(T)$ are finite dimensional subspaces, throughout let $\alpha(T)$ and $\beta(T)$ denote their dimensions.

The literature focuses on the case when there are the following assertions:
\begin{enumerate}[label=(\Alph*)]
  \item \label{ass-A}
    The values of $\alpha(T)$ and $\beta(T)$ are equal to some positive integer or zero.
  \item \label{ass-B}
    Equation~\eqref{eq11} is normally solvable in the sense of Hausdorff \cite{haussdorff}, i.e. for the existence of an element $f$ satisfying equation~\eqref{eq11}, it is necessary and sufficient to have:
    \begin{equation*}
       l(g) = 0  \quad \text{for all}~ l \in B(T).
    \end{equation*}
    Note that the necessity of this condition is trivial.
  \item \label{ass-C}
    $\alpha(T) = \beta(T)$.
\end{enumerate}

Sufficient conditions for the validity of these assertions are established by F. Riesz~\cite{riesz} and his followers as a generalization of the Fredholm theory for integral equations.
Several years ago, Nikolsky~\cite{nikolsky} found necessary and sufficient conditions under which the assertions \ref{ass-A}, \ref{ass-B}, \ref{ass-C} hold.

In this paper, we consider those cases where the operator $T$ satisfies conditions \ref{ass-A}, \ref{ass-B}, but is not subject to the condition~\ref{ass-C}.
In fact, we learn from an abstract point of view the known properties of singular integral equations,  whose theory is developed by Noether~\cite{noether}, Giraud~\cite{giraud}, Mikhlin~\cite{mikhlin} and Khalilov~\cite{khalilov}\footnote{From the editor:
A complete theory of one-dimensional singular integral equations is developed by
Muskhelishvili, Vekua, Kupradze and other Soviet mathematicians (see the monograph by Muskhelishvili:
``Singular integral equations''. Moscow---Leningrad, 1945).}.

Finally, we note that this applies to the theory of normed Gelfand rings.

\section{\texorpdfstring{}{§ 2}}
\setcounter{equation}{0}
The following theorem is a generalization of some of the results by Nikolsky.

\begin{thm}
  \label{thm-1}
  The following statements about the bounded linear operator $T$ are equivalent:

  \begin{itemize}
    \item[1a)]
      $\alpha(T)$ and $\beta(T)$ exist and are positive integers or zero;
    \item[1b)]
      the equation $Tf = g$ is always solvable with respect to $f$ if
      \begin{equation*}
        l(g) = 0 \quad \text{for all}~ l \in B(T).
      \end{equation*}
    \item[2a)]
      Same as 1(a);
    \item[2b)]
      the equation $lT = m$, where $m$ is a given bounded linear functional on $R$, is  always solvable with respect to $l$, if
      \begin{equation*}
        m(\varphi) = 0\quad \text{for all}~ \varphi\in A(T).
      \end{equation*}
    \item[3)]
      There is a bounded linear operator $U$, satisfying the relations
      \begin{equation*}
        TU = I - K_1, \quad UT = I - K_2,
      \end{equation*}
      where $I$ is the identity operator on $R$, and $K_1$, $K_2$ are finite-dimensional operators.
    \item[4)]
      There are bounded linear operators $U_1$ and $U_2$, such that $U_1T$ and $TU_2$ satisfy conditions~1a) and 1b).
    \item[5)]
      There are bounded linear operators $B, C, K$, such that
      \begin{equation*}
        T = B + K,
      \end{equation*}
      where $K$ is a finite dimensional operator, $C$ is a left or right inverse to the operator $B$ and $BC - CB$ is a finite-dimensional operator.
    \item[6)]
      There are bounded linear operators $B, C, V$, such that
      \begin{equation*}
        T = B + V
      \end{equation*}
      where $V$ is a compact operator, $C$ is a left or right inverse operator to the operator $B$ and $BC-CB$ is a compact operator.
  \end{itemize}
\end{thm}

\begin{proof}
  First, we point out that, as noted in the above-cited work of
  Nikolsky, the equivalence of assertions 1) and 2) is a consequence of some results by Hausdorff \cite{haussdorff}.

  Assertion 3) follows from assumption 1).

  Indeed, let the functionals $l_n\ (n = 1,2,\ldots,\beta(T))$ form a basis of the linear subspace $B(T)$.
  By linear independence of these functionals, we can determine, following Nikolsky, a system $g_n\ (n = 1,2,\ldots,\beta(T))$ of elements of $R$, satisfying the relations
  \begin{equation*}
    l_i(g_k) = \delta_{ik} \quad (i,k = 1,2,\ldots\beta(T)),
  \end{equation*}
  where $\delta_{ik}$ is the Kronecker symbol.
  Let $\varphi_n\ (n = 1,2,\ldots,\alpha(T))$ be elements of $R$, which form a basis of the linear subspace $A(T)$; define similarly a system of linear bounded functionals $m_n\ (n = 1,2,\ldots,\alpha(T))$, satisfying the relations
  \begin{equation*}
    m_i(\varphi_k) = \delta_{ik} \quad  (i,k = 1,2,\ldots,\alpha(T)).
  \end{equation*}
  Let $F$ and $G$ be the following subspaces of $R$: $F$ consists of those elements $f\in R$, for which
  \begin{equation*}
    m_n(f) = 0 \quad  (n = 1,2,\ldots,\alpha(T)),
  \end{equation*}
  or all elements of the form
  \begin{equation*}
    f - \sum\limits_{n=1}^{\alpha(T)} m_n(f) \varphi_n.
  \end{equation*}
  It is easy to verify, that these two definitions of the subspace $F$ are equivalent.
  From the first definition it follows immediately that $F$ is closed.
  Similarly, we define the closed subspace $G\subseteq R$, as the set of all elements $g\in R$, for which
  \begin{equation*}
    l_n(g)=0 \quad (n = 1,2,\ldots,\beta(T),
  \end{equation*}
  or as all elements of the form
  \begin{equation*}
    g - \sum\limits_{n=1}^{\beta(T)} l_n(g) g_n,
  \end{equation*}
  We assert that the operator $T$ is a bijective map from the subspace $F$ onto the subspace $G$.
  In fact, by condition~1b), for each $g\in G$ there is an element $f\in R$, such that $Tf = g$.
  Putting
  \begin{equation*}
    f' = f - \sum\limits_{n=1}^{\alpha(T)}  m_n(f)\varphi_n,
  \end{equation*}
  we note that then  $f'\in F$ and $Tf' = g$.
  This element $f'$ is uniquely determined, since assuming the contrary:
  \begin{equation*}
    f'' \in F, ~ Tf'' = g, ~ f' \neq f'',
  \end{equation*}
  we obtain:
  \begin{equation*}
    f' - f'' \in F \quad \text{and}~ T(f' -f'') = 0,
  \end{equation*}
  therefore, $f' - f''$ is represented as a linear combination of elements $\varphi_n\ (n = 1, 2,\ldots,\alpha(T))$ and simultaneously it holds
  \begin{equation*}
    m_n(f'-f'') = 0 \quad (n = 1, 2,\ldots,\alpha(T)),
  \end{equation*}
  i.e. we have a contradiction.

  Based on the well-known theorem of Banach (see, for example, \cite{schauder}), the inverse mapping of $G$ onto $F$ is also continuous.
  This mapping is itself a bounded linear operator.
  Denote this operator by $U$, with domain $G$.
  In general, for arbitrary $x\in R$ define:
  \begin{equation*}
    Ux = U\left(x - \sum_{n=1}^{\beta(T)}  l_n(x) g_n \right).
  \end{equation*}
  Since the functionals $l_n$ are continuous, $U$ is a continuous operator with domain $R$.

  It remains to verify that $U$ satisfies assertion 3) of
  our theorem.
  First, for $g\in G$ we have $TUg = g$; therefore, for any $x\in R$
  \begin{equation*}
    TUx
    = TU\left( x - \sum_{n=1}^{\beta(T)}  l_n(x) g_n\right)
    = x - \sum_{n=1}^{\beta(T)}  l_n(x) g_n,
  \end{equation*}
  or, in other words,
  \begin{equation*}
    TU = I - K_1,
  \end{equation*}
  where $K_1$ is a finite-dimensional operator.
  Secondly, for arbitrary $x\in R$ we have $Tx\in G$, so that $TUTx = Tx$, $UTx = x - \varphi$, where $\varphi$ is a linear combination $\varphi_n\ (n = 1,2,\ldots,\alpha(T))$.
  Given that $UTx \in F$, i.e. $x - \varphi \in F$, we get:
  \begin{equation*}
    \varphi = \sum_{n=1}^{\alpha(T)}  m_n(x) \varphi_n;
  \end{equation*}
  thus
  \begin{equation*}
    UT = I - K_2,
  \end{equation*}
  where $K_2$ is a finite dimensional operator.

  Assertion 4) follows from assumption 3).

  It suffices to note that the operators $I - K_1$ and $I - K_2$ satisfy the conditions~1a) and 1b), since we are dealing with easy cases of the theory of Riesz.

  Assertion 1) follows from assumption 4).
  First, we note that
  \begin{equation*}
    A(T) \subseteq A(U_1T),\quad B(T) \subseteq B(TU_2).
  \end{equation*}

  The finite dimensions of $A(T)$ and $B(T)$ are then a consequence of the finite dimensions of the subspaces $A(U_1T)$ and $B(TU_2)$.

  It remains to prove that the equation $Tf = g$ is normally solvable for $f$, i.e., there exists a solution $f$, if $l(g) = 0$ for all $l\in B(T)$.

  The assumptions about the operator $TU_2$ and the above stated results imply the existence of a bounded linear operator $W$, satisfying the relation
  \begin{equation*}
    TU_2W = I - K,
  \end{equation*}
  where $K$ is a finite-dimensional operator.
  Let $G_1$ be the set of all elements of the form $K\psi$, where $\psi$ runs over the space $R$, and $G_2$ be the subspace of $G_1$, consisting of those elements, which can also be represented as $Tx$, where $x\in R$.
  Let $\psi_n\ (n = 1, 2,\ldots,u)$ be a basis of $G_2$ and
  $\psi_n\ (n = 1, 2,\ldots,v)$ be a basis of $G_1$ so
  that $v\geq u$.
  Then we have:
  \begin{equation*}
    TU_2Wg = g - \sum_{n=1}^v c_n \psi_n,
  \end{equation*}
  where $c_n = c_n(g)\ (n = 1,2,\ldots,v)$ are bounded linear functionals.
  Consequently, there exists an element $f\in R$ satisfying the relation
  \begin{equation*}
    Tf = g - \sum_{n=u+1}^v c_n \psi_n,
  \end{equation*}

  where the functionals $c_n\ (n = u + 1,\ldots,v)$ satisfy
  \begin{equation*}
    c_n T =0 \quad (n = u+1,\ldots,v),
  \end{equation*}
  because otherwise there is a linear combination of elements
  $\psi_n~  (n = u+1,\ldots,v)$, represented in the form $Tx$, where $x\in R$ and, therefore, belongs to $G_2$.
  A contradiction.

  Assertion 5) follows from assumption 1).

  Indeed, let
  \begin{equation*}
    w = \inf\{\alpha(T), \beta(T)\},
  \end{equation*}
  \begin{equation*}
    T'x = \sum_{n=1}^{w} m_n(x)g_n,
  \end{equation*}
  \begin{equation*}
    U'x = \sum_{n=1}^{w} l_n(x)\varphi_n,
  \end{equation*}
  where $m_n, g_n, l_n, \varphi_n$ have the same values as before.
  It is easy to check that
  \begin{equation*}
    T'U = UT' = 0, \quad TU' = U'T =0,
  \end{equation*}
  \begin{equation*}
    T'U' = \sum_{n=1}^{w} l_n(x)g_n,
    \quad
    U'T' = \sum_{n=1}^{w} m_n(x)\varphi_n
  \end{equation*}
  If $w = \beta(T) \leq \alpha(T)$, we find:
  \begin{equation*}
    (T + T')(U + U') =  I,     \quad  (U + U') (T + T')   = I-K',
  \end{equation*}
  and if $w = \alpha(T) \leq \beta(T)$, then
  \begin{equation*}
    (T + T')(U + U') =  I-K'', \quad   (U + U') (T + T') = I,
  \end{equation*}
  where in both cases $K ', K''$ are finite-dimensional operators whose rank is equal to $\abs{\alpha(T) - \beta(T)}$.

  Putting $B = T + T'$, $C = U + U'$, $K = -T'$, we obtain Proposition 5) of Theorem~\ref{thm-1}.

  Assertion 6) follows from assumption 5), since finite-dimensional operators are compact.

  Assertion 4) follows from assumption 6), because the operators $CT$ and $TC$ satisfy 1a) and 1b), by virtue of the theory of Riesz.
  Theorem~\ref{thm-1} is proved.
\end{proof}


\section{\texorpdfstring{}{§ 3}}
\setcounter{equation}{0}
An operator satisfying the conditions of Theorem~\ref{thm-1}, is called a generalized Fredholm operator.
For these operators define the function $y(T)$ as follows:
\begin{equation*}
  y(T) = \alpha(T)-\beta(T).
\end{equation*}
Of course, the values taken by this function can only be positive integers or negative numbers or zero.
The latter case is the case of Fredholm, Riesz and Nikolsky.

In the next three theorems, we establish some properties of the function $y(T)$.

\begin{thm}
  \label{thm-2}
  Let $T_1$ and $T_2$ be generalized Fredholm operators.
  Then $T_1T_2$ is also a generalized Fredholm operator and it holds
  \begin{equation} \label{eq31}
    y(T_1T_2) = y(T_1) + y(T_2).
  \end{equation}
\end{thm}

\begin{proof}
  First, we check that $T_1T_2$ is a generalized Fredholm
  operator.
  By condition~3) of Theorem~\ref{thm-1}, there exist bounded linear operators $U_1$ and $U_2$, such that $T_1U_1 - I$, $U_1T_1 - I$, $T_2U_2 - I$, $U_2T_2 - I$ are finite-dimensional operators, so that
  \begin{equation*}
    U_2U_1T_1T_2 = I - K_3, \quad T_1T_2U_2U_1 = I - K_4
  \end{equation*}
  where $K_3$, $K_4$ are finite-dimensional operators, so that $T_1T_2$ satisfies condition~4) of Theorem~\ref{thm-1}.

  It remains to establish the validity of \eqref{eq31}.
  Calculate $\alpha(T_1T_2)$ and $\beta(T_1T_2)$.
  If $\varphi\in A(T_1T_2)$, there are two possibilities: either $\varphi\in A(T_2)$, or $T_2\varphi \in A(T_1)$, so that, if $T_2\varphi$ is denoted by $\psi$, we obtain:
  \begin{equation*}
    \psi \in A(T_1),~ l(\psi) = 0  \quad\text{for all}~ l \in B(T_2).
  \end{equation*}
  Let $A(T_1; T_2)$ denote the linear subspace of elements $\psi$ and by $\alpha(T_1; T_2)$ denote the dimension of this subspace: then we have:
  \begin{equation*}
    \alpha(T_1T_2) = \alpha(T_1) + \alpha(T_1; T_2)
  \end{equation*}
  and in the same way we obtain:
  \begin{equation*}
    \beta(T_1T_2) = \beta(T_1) + \beta(T_2; T_1),
  \end{equation*}
  where $\beta(T_2; T_1)$ is the dimension of $B(T_2; T_1)$, i.e., the linear subspace of functionals $l$ satisfying the relations
  \begin{equation*}
    l\in B(T_2) ~\text{and}~ l(\varphi) = 0 \quad\text{for all}~ \varphi \in A(T_1).
  \end{equation*}

  Let $\varphi_n\ (n = 1,2,\ldots,\alpha(T_1))$ be a basis of $A(T_1)$ and $l_n
  (n = 1,2,\ldots,\beta(T_2))$ be a basis of $B(T_2)$.
  Then every element $\psi \in A(T_1; T_2)$ is expressed as
  \begin{equation*}
    \psi =  \sum_{n=1}^{\alpha(T_1)}  a_n \varphi_n,
  \end{equation*}
  with
  \begin{equation} \label{eq32}
    \sum_{n=1}^{\alpha(T_1)}  a_n l_m(\varphi_n) = 0
    \quad  (m = 1,2,\ldots,\beta(T_2)),
  \end{equation}
  where $a_1,a_2,\ldots$ are constants.
  Similarly, an arbitrary linear functional $l\in B(T_2; T_1)$ is represented as
  \begin{equation*}
    l =  \sum_{n=1}^{\beta(T_2)}  b_n l_n
  \end{equation*}
  with
  \begin{equation} \label{eq33}
    \sum_{n=1}^{\beta(T_2)}  b_n l_n(\varphi_m) = 0
    \quad  (m = 1,2,\ldots,\alpha(T_1)).
  \end{equation}
  Then the numbers $\alpha(T_1; T_2)$ and $\beta(T_2; T_1)$ are equal to the largest number of linearly independent solutions of system~\eqref{eq32} and \eqref{eq33}, respectively, i.e. $\alpha(T_1) - r$ and $\beta(T_2) - r$, where $r$ is the rank of
  \begin{equation*}
    l_m(\varphi_n)\quad (m = 1,2,\ldots,\beta(T_2); n = 1,2,\ldots,\alpha(T_1)).
  \end{equation*}
  Hence
  \begin{align*}
    \alpha(T_1T_2) & = \alpha(T_2) + \alpha(T_1) - r, \\
    \beta(T_1T_2) & = \beta(T_1) + \beta(T_2) - r,
  \end{align*}
  which proves our theorem.
\end{proof}

\begin{thm}
  \label{thm-3}
  Let $T$ be a generalized Fredholm operator and V a compact operator.
  Then $T + V$ is a generalized Fredholm operator and it holds:
  \begin{equation*}
    y(T + V) = y(T).
  \end{equation*}
\end{thm}
\begin{proof}
  Let $U$ be the operator associated with the operator $T$, just as in condition~3) of Theorem~\ref{thm-1}.
  Then it holds:
  \begin{equation*}
    (T + V)U = I - K_1 + VU = I - V_1,
  \end{equation*}
  \begin{equation*}
    U(T + V) = I - K_2 + UV = I - V_2,
  \end{equation*}
  where $V_1$ and $V_2$ are compact operators.
  From condition~4) of Theorem~\ref{thm-1} and  the theory of Riesz, we deduce that $T + V$ is a generalized Fredholm
  operator.

  By Theorem~\ref{thm-2} and the theory of Riesz
  \begin{equation*}
    y(T + V) + y(U) = y((T + V) U) = y (I-V_1) = 0,
  \end{equation*}
  \begin{equation*}
    y(T) + y(U) = y(TU) = y(I - K_1) = 0
  \end{equation*}
  and consequently
  \begin{equation*}
    y(T + V) = y(T).
  \end{equation*}
\end{proof}

\emph{Remark:} If we take $T = I$, then our result will be a generalization of the Riesz theory.

\section{\texorpdfstring{}{§ 4}}
\setcounter{equation}{0}
Finally we prove the following property of the function $y(T)$:

If the function $y(T)$ is defined, then it is continuous in the neighborhood of the operator $T$ in the space of operators.
In other words, we have the following theorem:
\begin{thm}
  \label{thm-4}
  Let $T$ be  a generalized Fredholm operator.
  Then there exists a positive number $p$, with the following property: every bounded linear operator $T'$, satisfying the relation
  \begin{equation*}
    \norm{T' - T} < p,
  \end{equation*}
  is also a generalized Fredholm operator and
  \begin{equation*}
    y(T') = y(T).
  \end{equation*}
\end{thm}
\begin{proof}
  As in the proof of Theorem~\ref{thm-3}, we define the operator $U$ corresponding to the operator $T$.
  Then we have:
  \begin{equation*}
    T'U = TU + (T' - T)U = I + (T' - T)U - K_1,
  \end{equation*}
  \begin{equation*}
    UT' = UT + U(T' - T) = I + U(T' - T) - K_2.
  \end{equation*}
  Take $\norm{ T' - T} < \norm{U}^{-1}$, so that the operators
  \begin{equation*}
    I + (T '- T)U  ~\text{and}~  I + U(T' - T)
  \end{equation*}
  are invertible.
  According to the theory of Riesz, $T'U$ and $UT'$ are Fredholm operators and, in particular, they satisfy the conditions~1a) and 1b) of Theorem~\ref{thm-1}.
  This proves that $T'$ is a generalized Fredholm operator.

  Further, we have:
  \begin{equation*}
    y(T') + y(U) = y(T'U) = 0,
  \end{equation*}
  \begin{equation*}
    y(T)  + y(U) = y(TU) = y(I - K_1) = 0,
  \end{equation*}
  hence
  \begin{equation*}
    y(T') = y(T),
  \end{equation*}
  as asserted.
\end{proof}

We investigate the distribution of the set $S$ of all generalized Fredholm operators $T$ in the space $R_1$ of bounded linear operators on $R$.

The Theorem just proved indicates that $S$ consists of a set of connected open regions, inside each of which the function $y(T)$ is constant.
A boundary point of such a region cannot be a generalized Fredholm operator.
In fact, the results of Shilov~\cite{shilov} and Gelfand~\cite{gelfand} contain a proposal that all such points in the space of operators are a generalized zero-divisor, and hence, according to Hausdorff \cite{haussdorff}, the equation $Tf = g$ is not normal solvable.

Similar results hold for spectral theory.
Let $A$ be a bounded linear operator and $\lambda$ be a complex variable.
A generalized Fredholm domain is a set of values $\lambda$, for which $\lambda - A$ is a generalized Fredholm operator (see Nikolsky~\cite{nikolsky}).
This area consists of a countable set of connected open areas
in the complex plane, within each of which the function $y(\lambda I- A)$ is constant.
These areas are identified for some integral equations in Mikhlin~\cite{mikhlin}.

\section{\texorpdfstring{}{§ 5}}
\setcounter{equation}{0}

A remarkable result in the theory of singular integral equations was established by Mikhlin~\cite{mikhlin}, who determined a homeomorphism of some systems of singular operators onto a system of scalar functions.
A necessary and sufficient condition for the validity of the statements of Theorem~\ref{thm-1} for such an operator is that the corresponding function does not vanish in some region of the complex plane.
In this section we show how we can introduce these functions on the basis of abstract considerations.

First of all, we obtain a canonical representation for operators from the set $S$ of all generalized Fredholm operators.
Let $T\in S$.
Then, on the basis of Theorems~\ref{thm-1} and \ref{thm-2}, there exists an operator $U\in S$, satisfying the relation
\begin{equation*}
  y(T) = -y(U).
\end{equation*}
Let $a$ and $b$ be arbitrary integers, positive, negative or equal to zero, and $T_1, T_2$ be arbitrary operators belonging to the set $S$.
Then, by Theorem~\ref{thm-3} there exists an operator $T_3\in S$, satisfying the relation
\begin{equation*}
  y(T_3) = a y(T_1) + b y(T_2).
\end{equation*}
Hence, by well-known result in number theory, it follows that there exists an integer $r$, which is a divisor of all numbers $y(T)$, where $T\in S$.
Furthermore, there exist operators $X, Y\in S$, satisfying the relations
\begin{equation*}
  y(X) = r, ~ y(Y) = -r, ~ XY = I - K_1, ~  YX = I - K_2,
\end{equation*}
where $K_1$ and $K_2$ are finite-dimensional operators.

Now take an arbitrary operator $T$ from $S$ and write
\begin{equation*}
  y(T) = rs,
\end{equation*}
where $s$ is an integer.
If $s>0$, then Theorem~\ref{thm-2} implies that
\begin{equation*}
  y(TY^s) = 0.
\end{equation*}
According to the result of Nikolsky, as well as by virtue of a special case of Theorem~\ref{thm-1}, the operator $TY^s$ can be written as
\begin{equation*}
  TY^s = B + V,
\end{equation*}
where $B$ is invertible and  $V$ is a compact operator, and therefore
\begin{equation*}
  (B^{-1}T)Y^s = I + V_1,
\end{equation*}
where $V_1$, as well as the operators $V_2,V_3,\ldots$ (see below) are compact.
But the relation $YX = I - K_2$ easily implies that
\begin{equation*}
  Y^sX^s = I - V_2,
\end{equation*}
hence
\begin{equation*}
  B^{-1}T = X^s + V_3.
\end{equation*}
Similarly, we obtain for $s<0$:
\begin{equation*}
  B^{-1}T = Y^{-s} + V_4
\end{equation*}
and for $s = 0$
\begin{equation*}
  B^{-1}T = I + V_5.
\end{equation*}

The operator $B^{-1}T$ is called canonical representation of the operator $T\in S$.
Let $S_1$ denote the set of all operators $T'$, where $T'$ is the canonical representation of the operator $T\in S$.
Then every finite linear combination of elements of $S_1$ can be represented as
\begin{equation} \label{eq51}
  \sum_{n=1}^{\infty} a_n X^n+ \sum_{n=1}^{\infty} b_n Y^n + cI +V,
\end{equation}
where $a_1,a_2,\ldots,b_1,b_2\ldots$ and $c$ are complex constants and $V$ is a compact operator.

Consider the set $S_2$ of all absolutely convergent serieses of the form \eqref{eq51}.
Endowed with the norm
\begin{equation*}
  \sum_{n=1}^{\infty} \abs{a_n} \norm{X^n}
   + \sum_{n=1}^{\infty} \abs{b_n} \norm{Y^n}
   + \abs{c} + \norm{V}
\end{equation*}
the space $S_2$ becomes a normed ring.\footnote{Obviously, the product of two elements of the form \eqref{eq51} is determined by multiplying the corresponding terms of these elements.}

Let us find out, what elements of $S_2$ are generalized Fredholm operators.
This question can be answered with the theory of normed rings due to Gelfand.

We need the following

\begin{lem}
  The set $Z$ of all compact operators is a closed left and right ideal in the ring $R_1$ of bounded linear operators.
\end{lem}

\begin{proof}
  It is easy to see that the $Z$ is an ideal in $R_1$.
  Indeed, if $V_1, V_2\in Z$ and $T\in R_1$, then the operators $V_1 + V_2$, $V_1T$ and $TV_1$ are also compact.

  It remains to prove that $Z$ is a closed set, let $V_1,V_2,\ldots$ be a convergent sequence of compact operators such that $V_n\to W$ for some $W\in R_1$.
  We need to prove that $W$ is also a compact operator.
  Let $f_1,f_2,\ldots$ be an arbitrary bounded sequence in $R$.
  Then there is a sequence
  \begin{equation} \label{eq52}
    n_r^{(1)} \quad (r=1,2,\ldots),
  \end{equation}
  such that the sequence $V_1 f_{n_r^{(1)}}\ (r = 1,2,\ldots)$ converges and
  \begin{equation*}
    \norm{V_1 f_{n_r^{(1)}} - V f_{n_s^{(1)}}} < 2^{-1}.  \qquad \text{[sic!]}
  \end{equation*}
  There is also a subsequence $ n_r^{(2)}(r = 1,2,\ldots)$ of the
  sequence in \eqref{eq52}, such that the sequence $V_2 f_{n_r^{(2)}}\ (r = 1,2,\ldots)$ converges and
  \begin{equation*}
    \norm{V_2 f_{n_r^{(2)}} - V_2 f_{n_s^{(2)}}} < 2^{-2},
  \end{equation*}
  etc.
  Consider the sequence
  \begin{equation*}
    W f_{n_r^{(r)}} \quad (r=1,2,\ldots).
  \end{equation*}
  We assert that this sequence converges.
  Indeed for $r\leq s$, we have:
  \begin{multline*}
    \norm{W f_{n_r^{(r)}} - W f_{n_s^{(s)}}}
    = \norm{V_r f_{n_r^{(r)}} - V_r f_{n_s^{(s)}} + (W-V_r)(f_{n_r^{(r)}} - f_{n_s^{(s)}})}
    \leq \\
    \leq 2^{-r} + \norm{W- V_r}\left(\norm{f_{n_r^{(r)}}\norm + \norm f_{n_s^{(s)}}}\right)\to 0
    \quad\text{for}~ r,s\to\infty.
  \end{multline*}
  Hence, by the completeness of the space $R$, it follows that the sequence $W f_n\ (n = 1,2,\ldots)$ contains a convergent subsequence.
  In other words, $W$ is a compact operator.
  The Lemma is proved.
\end{proof}

Now it can be argued on the basis of Gelfand~\cite{gelfand} that the quotient ring $S_2/Z$ is also a normed ring, where the norm is defined as follows.
For $T\in S_2$ the norm of $T$ in the quotient ring $S_2/Z$ is
\begin{equation*}
  \inf_{v\in Z} \norm{T - V}.
\end{equation*}
In the ring $S_2/Z$ the zero element is the set of compact
operators, and the unit element is the set of elements of the form $I + V$, i.e. operators of Fredholm and Riesz type.
It is easy to verify that $S_2/Z$ is a commutative ring.
Indeed, the elements belonging to $S_2$, are linear combinations of the following elements:
\begin{equation*}
  V,\ I+V, X^n +V,\ Y^n +V,
\end{equation*}
where $V\in Z$ and $n$ is a positive integer.
In any case, for $T_1, T_2 \in S_2$, $T_1T_2 - T_2T_1$ is a compact operator, since $X^n Y^n - I$ and $Y^n X^n - I$ are compact operators.

Thus, $S_2/Z$ is a normed commutative ring with two generators $X'$ and $Y'$ where $X'$ and $Y'$ are  elements of the ring $S_2/Z$, corresponding to the operators $X$ and $Y$.
Consequently,
\begin{equation*}
  X'Y' = I',
\end{equation*}
where $I'$ is the unit of the ring $S_2/Z$.
Such rings have recently been considered by Shilov (\cite{shilov}, p.~29).
Each element $T'$ of the ring $S_2/Z$ is expressed as an absolutely convergent series:
\begin{equation*}
  T' =   \sum_{-\infty}^{\infty}  a_n X'^n.
\end{equation*}

According to the theory of Gelfand, there is a region $\Gamma$ in the complex plane, such that the necessary and sufficient conditions for the invertibility of an element $T'$ in the ring $S_2/Z$ is a nonvanishing number $\sum\limits_{-\infty}^{\infty}a_n z^n$ for
some $z\in \Gamma$.
If $T\in S_2$ and $T'$ is an element of $S_2/Z$,
corresponding to the operator T, then this condition is a condition that $T$ is a generalized Fredholm operator.

\begin{center}
  \footnotesize{(Received 2 / IX 1948)}
\end{center}

\phantomsection
\addcontentsline{toc}{section}{References}

\begin{thebibliography}{10}
\bibitem[1]{riesz}{\sc F.~Riesz: }{\em On linear functional equations.}
  \newblock {Uspekhi Mat. sciences., \textbf{1} (1936) 175 -- 199.}
\bibitem[2]{haussdorff}{\sc F.~Hausdorff: }{\em Zur Theorie der linearen metrischen Räume.}
  \newblock {Journ. reine und angew. Math.,  \textbf{167} (1932), 294 -- 311.}
\bibitem[3]{nikolsky}{\sc C. M.  Nikolsky: }{\em Linear equations in normed linear spaces.}
  \newblock {Math. SSSR, Ser. Math., v.  \textbf{7}, No. \textbf{3} (1943), 147 -- 166.}
\bibitem[4]{noether}{\sc F. Noether: }{\em Über eine Klasse singulärer Integralgleichungen.}
  \newblock {Math. Ann., \textbf{82} (1921), 42 -- 63.}
\bibitem[5]{giraud}{\sc J. Giraud: }{\em {\'{E}}quations {\`{a}} integrales principales.}
  \newblock {Ann. Ecole Norm. Sup., \textbf{51} (1934), 251 -- 372.}
\bibitem[6]{mikhlin}{\sc S.G. Mikhlin: }{\em Singular integral equations with two independent variables strap.}
  \newblock {Mat. Sat., \textbf{1 (43)} (1936), 535 -- 551; Problem of equivalence in the theory of singular equations, Mat. Sat., \textbf{3 (45)} (1938), 121 -- 141.}
\bibitem[7]{schauder}{\sc J. Schauder: }{\em Über die Umkehrung linearer stetiger Funktionaloperationen.}
  \newblock {Studia Math., \textbf{2} (1930), 1 -- 6.}
\bibitem[8]{gelfand}{\sc I. M. Gelfand: }{\em Normed rings.}
  \newblock {Mat. Sat., \textbf{9 (51)} (1941), 3 -- 23.}
\bibitem[9]{shilov}{\sc G. E. Shilov: }{\em On normed rings with one generator.}
  \newblock {Mat. Sat., \textbf{21 (63)} (1947), 25 -- 47.}
\bibitem[10]{khalilov}{\sc I. X. Khalilov: }{\em Linear singular equations in a normed ring.}
  \newblock {Math. Akad Mat., V. \textbf{13}, No. \textbf{2} (1949), 163 -- 176.}
\end{thebibliography}

\end{document}
