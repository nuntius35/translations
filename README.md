# README

This repository contains English translations of the papers

* F. V. Atkinson: _The normal solubility of linear equations in normed spaces_.
Mat. Sbornik N.S. (__28(70)__, №1), 1951, 3–14.
* Ya. Sinaĭ: _The notion of entropy of a dynamical system_.
Dokl. Akad. Nauk SSSR (__124__, №4)), 1959, 768–771.
* Ya. Sinaĭ: _Flows with finite entropy_.
Dokl. Akad. Nauk SSSR (__125__, №6), 1959, 1200–1202.

[Homepage of Andreas Geyer-Schulz](https://nuntius35.gitlab.io)
