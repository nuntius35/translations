\documentclass[11pt]{article}

\usepackage{cmap} %make characters in pdf copyable
\usepackage[a4paper, textwidth=15cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T2A]{fontenc}
\usepackage[russian]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage[final,
            activate={true,nocompatibility},
            babel,
            kerning=true,
            spacing=true,
            factor=1700,
            stretch=20,
            shrink=50]
            {microtype}
\usepackage[all]{nowidow}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{bookmark}
\usepackage{enumitem}

\newcommand{\abs}[1]{\left\lvert{#1}\right\rvert}
\newcommand{\norm}[1]{\left\lVert{#1}\right\rVert}

\renewcommand{\thesection}{\S~\arabic{section}.}
\newtheorem{thm}{Теорема}
\newtheorem*{cor}{Следствие}
\newtheorem{lemma}{Лемма}
\newtheorem*{defi}{Определение}

\hypersetup{
  pdfinfo={
      Title={О ПОНЯТИИ ЭНТРОПИИ ДИНАМИЧЕСКОЙ СИСТЕМЫ},
      Author={Я. СИНАЙ}
  }
}

\begin{document}

\author{Я. СИНАЙ}
\title{О ПОНЯТИИ ЭНТРОПИИ ДИНАМИЧЕСКОЙ СИСТЕМЫ}
\date{}
\maketitle


\section{\texorpdfstring{}{§ 1}}\label{intro}

В настоящей заметке дается определение энтропии, пригодное для произвольных автоморфизмов пространства Лебега.
Доказываемая затем теорема~\ref{thm1} указывает в некоторых случаях путь к ее вычислению.
На основе этой теоремы получаются новые метрические инварианты некоторых эргодических автоморфизмов компактных коммутативных групп.

\section{\texorpdfstring{}{§ 2}}\label{main1}

Пусть $M$ --- пространство Лебега с $\sigma$-алгеброй измеримых множеств и мерой $\mu$, $\mu(M) = 1$; $T$ --- произвольный автоморфизм этого пространства \cite{rokhlin-a}.
Под конечным разбиением $A=\{A_1,\ldots,A_n\}$ пространства $M$ понимается его представление в виде суммы не пересекающихся множеств \footnote{Все рассматриваемые множества предполагаются измеримыми.} $M=\cup_{i=1}^n A_i$.
Под разбиением $T^k A$ понимается разбиение, образованное множествами $\{T^k A_i\},~i=1,\ldots,n$.

Энтропия произвольного конечного разбиения $A$ дается известной формулой
\begin{equation*}
  h(A) = -\sum_{i=1}^n \mu(A_i) \log \mu(A_i).
\end{equation*}

В соответствии с этой формулой мы можем найти значение энтропии разбиения $A \vee  T A \vee \ldots \vee T^n A$, образованного всевозможными множествами вида $A_{l_0} \cap T A_{l_1} \cap \ldots T^n A_{l_n}$.
Из общих теорем теории информации (\cite{khinchin}) следует, что для любого конечного разбиения $A$ существует предел
\begin{equation*}
  \lim_{k\to\infty} = \frac{h(A\vee T A \vee \ldots \vee T^k A)}{k} = h_T{A}.
\end{equation*}

\begin{defi} Энтропией автоморфизма $T$ будем называть верхнюю грань $h_T(A)$ по всем конечным разбиениям $A$: $h_T = \sup_{A} h_T(A)$.
\end{defi}

Рассмотрим два произвольных разбиения $A=\{A_1 \ldots A_n\}$ и $B=\{B_1 \ldots B_l\}$.
Предположим, что множества $B_i$ принадлежат замкнутой $\sigma$-алгебре, порожденной множествами $\{T^n A_j, 1\leq j \leq k, -\infty<n<\infty\}$.
Тогда Справедлива следующая теорема:
\begin{thm} \label{thm1}
  Для разбиений $A$ и $B$, обладающих указанным Свойством, выполняется неравенство $h_T(B) \leq h_T(A)$.
\end{thm}
Отметим хорошо известные свойства энтропии, на которые мы будем опираться.
Пусть $K$, $L$, $M$ ---  произвольные конечные разбиения.
Тогда
\begin{itemize}
  \item[1)] $h(K\vee L) = h(K) + h(L \mid K)$;
  \item[2$\alpha$)] $h(K) \leq h(K\vee L) \leq h(K) + h(L)$;
  \item[2$\beta$)] $h(K\vee L \mid M) \leq h(K \mid M) + h(L \mid M)$;
  \item[3)] $h(K \mid L)\geq h(K \mid M)$,
\end{itemize}
если элементы разбиения $L$ можно представить как суммы элементов разбития $M$.

\begin{proof}
  На основании свойств: 1) и 2$\alpha$)
  \begin{multline}\label{eq-1}
    h(B\vee T B \vee \ldots \vee T^r B) \leq \\
    \leq h(B\vee T B \vee \ldots \vee T^r B \vee T^{-n}A \vee \ldots \vee T^n A \vee \ldots \vee T^{n+r}A) = \\
    = h(T^{-n}A \vee \ldots \vee T^{n-r} A) + h(B\vee T B \vee \ldots \vee T^r B \mid T^{-n}A \vee \ldots \vee T^{n+r}A).
  \end{multline}

  Далее, на основании свойств 2$\beta$) и 3)
  \begin{multline}\label{eq-2}
    h(B \vee T B \vee \ldots \vee T^r B \mid T^{-n} A \vee \ldots \vee T^{n+r} A) \leq\\
    \leq \sum_{i=0}^r h(T^i B \mid T^{-n} A \vee \ldots \vee T^{n+r} A) \leq\\
    \leq \sum_{i=0}^r h(T^i B \mid T^{-n+i} A \vee \ldots \vee T^n A) = (r+1) h(B \mid T^{-n}A\vee\ldots\vee T^n A).
  \end{multline}
  Легко показать, что, в силу нашего условия на разбиения $A$ и $B$, для любого $\varepsilon>0$ $h(B \mid T^{-n}A\vee\ldots\vee T^n A) <\varepsilon$ при достаточно большом $n$.
  Деля обе части неравенства \eqref{eq-1} на г и пользуясь последним утверждением и неравенством \eqref{eq-2},
  получим
  \begin{equation*}
    \frac{h(B\vee T B \vee \ldots\vee T^r B)}{r}
    \leq \frac{h(A\vee\ldots\vee T^{2n+r}A)}{2n+r}\frac{2n+r}{r} + \varepsilon\frac{r+1}{r}.
  \end{equation*}


  Поскольку $n$ зависит только от $\varepsilon$, то, переходя к пределу при $r\to\infty$‚ получим, ввиду произвольности $\varepsilon$, требуемый результат.
\end{proof}

\begin{cor}
  Если разбиение $A$ таково, что замкнутая $\sigma$-алгебра, порожденная множествами $\{T^k A_i\}$, $-\infty<k<\infty$, $1\leq i\leq k$, совпадает с $S$, то $h_T = h_T(A)$.
\end{cor}

\begin{thm}
  Если разбиение $A$ таково, что минимальная $\sigma$-алгебра, содержащая множества $\{T^k A_i\}$ при $k\geq 0$, совпадает с $S$, то $h_T =0$.
\end{thm}

Доказательство основано на том факте, что в наших условиях $h(A \mid T A\vee \ldots \vee T^n A)\to 0$ про $n\to\infty$.

\begin{thm}
  Для любого автоморфизма $T$ имеет место равенство $h_{T^k} = \abs{k} h_T$.
\end{thm}

\begin{proof}
  Для любого разбиения $A$ имеем
  \begin{equation*}
    \frac{h(A\vee\ldots \vee T^{n k}A)}{n \abs{k}} \geq \frac{h(A\vee T^k A \vee \ldots\vee T^{n k}A)}{n \abs{k}},
  \end{equation*}
  или, переходя к пределу при $n\to\infty$, $h_T(A) \geq \tfrac{1}{k}h_{T^k}(A)$.

  Из последнего неравенства следует, что $h_T \geq \tfrac{1}{k}h_{T^k}(A)$.
  Пусть теперь $h'$ --- любое число, меньшее $h_T$.
  Тогда найдется разбиение $A$ такое, что $h_T(A)\geq h'$.
  Рассмотрим разбиение $B = A\vee T A \vee \ldots \vee T^{k-1} A$.
  Очевидно, что $h(B\vee T^k B \vee \ldots\vee T^{k(n-1)}B) = h(A\vee T A \vee \ldots \vee T^{k n-1} A)$ и
  \begin{equation*}
    h_{T^k} \geq \frac{h(B\vee T^k B \vee \ldots \vee T^{k(n-1)}B)}{n} = \frac{h(A\vee T A\vee\ldots \vee T^{k n-1} A)}{\abs{k}n} \abs{k}.
  \end{equation*}
  Предельный переход при $n\to\infty$ дает $h_{T^k}\geq h'\abs{k}$.
  Ввиду произвольности $h'<h_T$ теорема доказана.
\end{proof}

\section{\texorpdfstring{}{§ 3}} \label{main2}

Используем доказанные теоремы для вычисления энтропии эргодического анаморфизма двумерного тора.
Такой автоморфизм задается целочисленной матрицей $2$-го порядка с определителем $\pm1$.
Условие его эргодичности состоит в существовании двух действием собственных эначений (см. \cite{khinchin}).

Представим тор в виде плоскости $z=(x,y)$, у которой отождествлены точки, обе координаты которых отличаются между собой на целое число.
Будем обозначать через $\bar{T}$ линейное преобразование плоскости и через $T$ --- соответствующее преобразование тора.
Очевидно, что найдется такое $k$, что для любых пар точек $z_1=(x_1,y_1)$, $z_2=(x_2,y_2)$, для которых $\abs{x_1-x_2} < \tfrac{1}{k}$ $\abs{y_1-y_2} < \tfrac{1}{k}$, выполняется неравенство $\norm{\bar{T} z_1 - \bar{T} z_2} \leq \tfrac{1}{3}$
(под нормой можно понимать обычную эвклидову длину).
Возьмем в качестве исходного разбиение $A$, образованное множествами $A_{ij} = \left\{ \tfrac{i}{k} \leq x < \tfrac{i+1}{k}, \tfrac{i}{k} \leq y < \tfrac{i+1}{k}\right\}$.
Легко показать, что сдвиги $T^n A$ такого разбиения образуют базис.
На основании следствия к теореме~\ref{thm1} имеем $h_T = h_T(A)$.

Сделаем следующее общее замечание.
Пусть $\zeta$ --- произвольное измеримое разбиение пространства $M$ и $D$ --- конечное разбиение пространства $M$.
Тогда для любого элемента разбиения $C\in\zeta$
\begin{multline}\label{eq-3}
  h(A\vee\ldots\vee T^k A \mid  D \mid C) \leq h(A\vee\ldots\vee T^k A \mid  C) \leq \\
  \leq h(D \mid C) +  h(A\vee\ldots\vee T^k A \mid  D \mid C).
\end{multline}

Для дальнейшего нам понадобятся следующие две элементарные леммы, доказательство которых мы опустим.
\begin{lemma}\label{lem1}
  Для любого $\delta>0$ существует такое $L(\delta)$, что на любом отрезке прямой $y=A x+B$, длина которого больше $L(\delta)$, найдется точка, отстоящая от некоторой точки с координатами $(B+i,j)$, где $i,j$ --- целые числа, на расстояние, меньшее $\delta$.
\end{lemma}
\begin{lemma}\label{lem2}
Для параллелограмма с вершинами $(0,0)$, $(x,A x)$, $(c,d)$, $(c+x,A x+d)$, число точек с целочисленными координатами, содержащихся внутри параллелограмма, при достаточно малом $\sqrt{c^2+d^2}$ не превышает $x$.
\end{lemma}

Примем собственные прямые $l^1$, $l^2$ автоморфизма $T$ за новые координатные оси, обозначаемые в дальнейшем через $\tilde{x}, \tilde{y}$.
Соответствующие собственные значения обозначим через $\lambda_1$, $\lambda_2$ ($\abs{\lambda_1} = \abs{\lambda_2}^{-1} > 1$).

Рассмотрим разбиение $\gamma$ квадрата $E=\{0\leq x\leq 1, 0\leq y \leq 1\}$ на отрезки $l$, параллельные прямой $l^2$ и оканчивающиеся на сторонах квадрата $E$ \footnote{Идея применить разбиение т принадлежит А. Н. Колмогорову.}.
Измельчением разбиения $\gamma$ является разбиение $\gamma_\varepsilon$‚ получаемое следующим образом:
каждый отрезок $l$ делится точками вида $j\cdot\varepsilon$ на отрезки $L_\varepsilon$ длины $\varepsilon$ и на не более чем два отрезка длины, меньшей $\varepsilon$.

Пусть $z_1 = (\tilde{x}, m\varepsilon)$ и $z_2=(\tilde{x}, (m+1)\varepsilon)$ --- концы некоторого отрезка $l_\varepsilon$.
Тогда $T^{-n}l_\varepsilon$ представляет собой отрезок, координаты концов которого имеют вид:
$T^{-n} z_1 = (\tilde{x}\lambda_1^{-n}, m\varepsilon\lambda_2^{-n})$, $T^{-n} z_2 = (\tilde{x}\lambda_1^{-n}, (m+1)\varepsilon\lambda_2^{-n})$.

Применяя лемму~\ref{lem1}, получим, что при сколь угодно малых $\delta_1$,$\delta_2$, $\varepsilon$ и достаю большом $n$, зависящем от $\delta_1$,$\delta_2$,$\varepsilon$, для каждого отрезка $l_\varepsilon$ длины $\varepsilon$ найдется содержащийся в нем отрезок $\widetilde{l_\varepsilon}$ постоянной длины $\varepsilon' > (1-\delta_2)\varepsilon$ и такой, что $T^{-n} \widetilde{l_\varepsilon}$ есть отрезок, координаты $(\tilde{x},\tilde{y})$ концов которого имеют вид $(\alpha,0)$ и $(\alpha,\varepsilon'\lambda_2^{-n})$, где $\abs{\alpha} < \delta_1$.

Пользуясь леммой~\ref{lem2}, нетрудно показать, что из каждого отрезка $\widetilde{l_\varepsilon}$ можно выкинуть не более чем счетное число отрезков, общая условная мера которых при условии\footnote{Понятие условной меры и условного распределения тождественно ???} $\widetilde{l_\varepsilon}$ не превосходит $\tfrac{c\delta_1\abs{\lambda_1}}{\abs{\lambda_1}-1}$, $c$ --- некоторый коэффициент, зависящий от тангенса угла наклона прямой $l^2$ к оси $x$, так что на оставшемся множестве условное распределение разбиения $A\vee\ldots\vee T^{s}A$ не зависит от множества
$T^{-n} \widetilde{l_\varepsilon}$.

Итак, мы получаем следующую картину:
на каждого отрезка $l_\varepsilon$ длины можно выкинуть множество меры не большей $\varepsilon\left(\delta_2 + \tfrac{c\delta_1\abs{\lambda_1}}{\abs{\lambda_1}-1}\right)$ так, что на оставшемся множестве $\tilde{\tilde{l_\varepsilon}}$ условное распределение разбиения $T^n A \vee \ldots \vee T^{n+s}A $ при достаточно большом $n$ не зависит от множества $\tilde{\tilde{l_\varepsilon}}$.

Пусть множество $M_1$ есть объединение таких множеств:
1) множеств $l_\varepsilon - \tilde{\tilde{l_\varepsilon}}$;
2) отрезков длины, меньшей $\varepsilon$, получающихся при делении отрезков $l$.
Тогда $\mu(M_1) \leq 8\varepsilon + \delta_2 + \tfrac{c\delta_1\abs{\lambda_1}}{\abs{\lambda_1}-1} = \delta$.
Возьмем разбиение $D$ тора на множества $M_1$ и $M_2 = M - M_1$.
Беря в формуле \eqref{eq-3} в качестве $\zeta$ тривиальное разбиение, легко получим
\begin{equation}\label{eq-4}
  (1-\delta)\tfrac{h(A\vee\ldots\vee T^s A \mid M_2)}{s} \leq \tfrac{h(A\vee\ldots\vee T^s A)}{s}
  \leq \tfrac{h(D) + h(A\vee\ldots\vee T^s A \mid M_2)}{s} + 2\delta\log k.
\end{equation}

Беря в \eqref{eq-3} в качестве $\zeta$ разбиение $\gamma$, получим для отрезка $l\in M_1$
\begin{multline}\label{eq-5}
  (1-\delta)\frac{h(A\vee\ldots\vee T^s A \mid M_2\cap l)}{s} \leq \frac{h(A\vee\ldots\vee T^s A \mid l)}{s} \leq \\
  \leq \frac{h(D \mid l) + h(A\vee\ldots\vee T^s A \mid M_2\cap l)}{s} + 2\delta\log k.
\end{multline}
Как показано в $h(T^n A \vee \ldots \vee T^{n+t}A \mid M_2) = h(T^n A \vee \ldots \vee T^{n+t}A \mid M_2 \cap l)$ для $l\in M_1$.
Поэтому, проинтегрировав \eqref{eq-5} по $l$ и воспользовавшись неравенством
\begin{multline}\label{eq-6}
  h(T^n A \vee \ldots \vee T^s A \mid  M_2\cap l)
  \leq h(A \vee \ldots \vee T^s A \mid  M_2\cap l) \\
  \leq h(T^n A \vee \ldots \vee T^s A \mid  M_2\cap l) + h(A \vee \ldots \vee T^n A \mid  M_2\cap l)
\end{multline}
при $n<s$, получим при достаточно большом $s$
\begin{multline}\label{eq-7}
  \frac{(1-\delta)h(T^n A \vee \ldots \vee T^s A \mid  M_2)}{s} \leq \frac{h(A \vee \ldots \vee T^s A \mid  \gamma)}{s} \leq \\
  \leq \frac{h(D \mid \gamma) + h(T^n A \vee \ldots \vee T^s A \mid  M_2)}{s} + 2(\tfrac{n}{s} + \delta) \log k.
\end{multline}

Заметим, что $n$ зависит только от $\delta$.
Поэтому, полагая в \eqref{eq-4} и \eqref{eq-7} $s\to\infty$, получим основное равенство:
\begin{equation*}
  h_T(A) = \lim_{s\to\infty} \frac{h(A\vee T A\vee\ldots\vee T^{s}A \mid \gamma)}{s}.
\end{equation*}

Доказательство того, что $\lim_{s\to\infty} \tfrac{h(A\vee\ldots\vee T^{s}A \mid l)}{s} = \log\abs{\lambda_1}$‚ требует элементарных геометрических соображений, которые мы опускаем.

Мы получаем, таким образом, новый метрический инвариант для автоморфизмов двумерного тора --- модуль собственного значения.

Для эргодических автоморфизмов $k$-мерного тора, имеющих $k$ действительных собственных значений, изложенные выше соображения обобщаются без Труда.
Энтропия их дается формулой
\begin{equation*}
  h_T = \sum_{\abs{\lambda_i} > 1} \log \abs{\lambda_i}
\end{equation*}

В заключение выражаю благодарность А. Н. Колмогорову и В. А. Рохлину за ценные обсуждения рассмотренных Здесь задач.

\phantomsection
\addcontentsline{toc}{section}{Список литературы}

\begin{thebibliography}{10}
\bibitem[1]{kolmogorov}{\sc А. Н. Колмогоров,}{}
                 \newblock{ДАН, \textbf{119}, \textnumero\ 5 (1958).}
\bibitem[2]{rokhlin-a}{\sc В. А. Рохлин,}{}
                 \newblock{Усп. матем. наук, \textbf{4}, 2 (30) (1949).}
\bibitem[3]{rokhlin-b}{\sc В. А. Рохлин,}{}
                 \newblock{Иев АН СССР, сер. матем., \textbf{13}, 329 (1949).}
\bibitem[4]{khinchin}{\sc А. Я. Хинчин,}{}
                 \newblock{Усп. матем. наук, \textbf{11}, \textnumero\ 1 (67) (1949).}

\end{thebibliography}
\end{document}
