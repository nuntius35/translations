all: buildpdf

buildpdf:
	latexmk -pdf -silent -time

clean:
	latexmk -pdf -c

cleanall:
	latexmk -pdf -C

check:
	java -jar ~/Downloads/textidote/textidote.jar --remove flalign,otherlanguage --remove-macros index --ignore sh:c:noin,sh:nobreak,lt:en:MULTIPLICATION_SIGN,lt:en:UPPERCASE_SENTENCE_START --check en_UK --dict spell.txt --output html diss.tex > report.html
