\documentclass[11pt, a4paper]{article}

\usepackage{cmap} %make characters in pdf copyable
\usepackage[utf8]{inputenc}
\usepackage[T2A]{fontenc}
\usepackage[russian]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage[final,
            activate={true,nocompatibility},
            babel,
            kerning=true,
            spacing=true,
            factor=1700,
            stretch=20,
            shrink=50]
            {microtype}
\usepackage[all]{nowidow}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{bookmark}
\usepackage{enumitem}


\renewcommand{\thesection}{\S~\arabic{section}.}
\newtheorem*{thm}{Теорема}

\hypersetup{
  pdfinfo={
      Title={О ПОТОКАХ С КОНЕЧНОЙ ЭНТРОПИЕЙ},
      Author={Я. СИНАЙ}
  }
}

\begin{document}

\author{Я. СИНАЙ}
\title{О ПОТОКАХ С КОНЕЧНОЙ ЭНТРОПИЕЙ}
\date{}
\maketitle


\section{\texorpdfstring{}{§ 1}}\label{intro}

Рассмотрим поток $(M, \mathfrak{S}, S^t)$, где $M$ --- пространство Лебега с мерой $\mu$; $\mathfrak{S}$ --- $\sigma$-алгебра его измеримых множеств и $S^t$, $-\infty <t< \infty$, --- действующая в этом пространстве группа сохраняющих меру преобразований (см. \cite{rokhlin}).
Для отдельных автоморфизмов этой группы можно ввести согласно \cite{sinai} энтропию $h_{S^\Delta} = h_S(\Delta)$.
Во всех известных примерах зависимость этой функции от $\Delta$ оказывается линейной, но доказательство этого фака в об щем случае отсутствует.
Поэтому будем называть энтропией потока $S^t$
\begin{equation*}
  \sup_{\Delta>0} \frac{h_S(\Delta)}{\Delta} = h_S.
\end{equation*}

Для вычисления энтропии потоков удобно пользоваться следующей теоремой, справедливой для произвольного автоморфизма $T$.
\begin{thm}
  Пусть дана последовательность разбиений $\{g_k\}$ такая, что $g_k\leq g_{k+1}$,
  \begin{equation*}
    \prod\limits_{k} \prod\limits_{n=-\infty}^\infty \{T^n g^k\}
    = \varepsilon,
  \end{equation*}
  где $\varepsilon$ --- разбиение на отдельные точки и $h(g_k) <\infty$.
  Тогда
  \begin{equation*}
    h_T
    = \lim_{k\to\infty} h_T(g_k).
  \end{equation*}
\end{thm}

Доказательство этой теоремы аналогично доказательству теоремы~1 в \cite{sinai}.

В настоящей заметке показывается, что существуют транзитивные потоки со счетнократным лебеговским спектром и любым конечным $h_S >0$.
Примеры потоков с $h_S = \infty$ тривиальны.
Пример из \ref{2ndexample} был опубликован в работе А. Н. Колмогорова \cite{kolmogorov}.
Примеры \ref{1stexample} также принадлежат Колмогорову.

\section{\texorpdfstring{}{§ 2}}\label{1stexample}

Рассмотрим марковский процесс, у которого фазовым пространством служат два отрезка $A_1 B_1$ и $A_2 B_2$ длины соответственно $\alpha$ и $\beta$.
Внутри каждого отрезка происходит детерминированное движение слева направо с единичной скоростью.
Из правого конца каждого отрезка ($B_1$ или $B_2$) с вероятностями $1/2$‚ $1/2$ происходит скачок в точки $A_1$ или $A_2$.
Известно, что задание локальных характеристик движения вместе с указанием начального распределения определяет единственным образом меру в пространстве траекторий марковского процесса.
Если взять в качестве начального распределения равномерное распределение на обоих отрезках, то получившийся марковский процесс будет стационарным.

Для вычисления энтропии получаемого таким образом потока нужно вычислить энтропию соответствующих ему автоморфизмов $S^\Delta$.
Разобьем отрезок $A_1 B_1$ на отрезки длины $\alpha/2^k$.
Отрезок $A_2 B_2$ разобьем на отрезки той же длины $\alpha/2^k$, кроме, быть может, последнего.
Мы получим всего $2^k [1+\tfrac{\beta}{\alpha} +1]$ отрезков $I^k_l$.
Пусть $\{g_k\}$ есть разбиение всего пространства на множества $G^k_l$, образованные траекториями, находящимися в момент времени $0$ в отрезках $I^k_l$.
Легко видеть, что при $\Delta <\min(\alpha,\beta)$ последовательность разбиений $g^k$ удовлетворяет условиям теоремы.

Рассмотрим, что происходит с множествами $G^k_l$ при применении к ним преобразовании $S^{\Delta n}$.
Иными словами, нужно рассмотреть, как разбиваются множества $G^k_l$ в зависимости от возможных положении траектории марковского процесса в моменты времени $0,\Delta,\ldots,n\Delta,\ldots$.
В случае детерминированного движения траекторий за время от $0$ до $\Delta$ отдельное множество $G^k_l$ развивается на не более чем два множества.
В случае множеств $G^k_l$, состоящих из траектории, делающих скачок за время от $0$ до $\Delta$, множество $G^k_l$ распадается на сумму двух множеств $G^k_{l_1}$ и $G^k_{l_2}$ условной меры по $1/2$ в зависимости от того, на каком из отрезков $A_1B_1$ или $A_2B_2$ траектория окажется в момент времени $\Delta$.
Дальнейшее изучение разбиения можно проводить для каждого из множеств $G^k_{l_1}$ и $G^k_{l_2}$ независимо.
Некоторые множества $G^k_l$ могут быть такими, что одна часть траекторий должна делать скачок за время от $0$ до $\Delta$, а другая часть двигается детерминировано.
Разбиение в этом случае является комбинацией предыдущих двух и ничем от них не отличается.

За $n$ шагов множество $G^k_l$ разобьется на отдельные подмножества в зависимости от числа скачков траекторий.
Условная мера каждого такого подмножества равна $2^{-\nu}$, где $\nu$ --- число скачков.
Кроме того, каждое подмножество с фиксированным числом скачков разобьется еще на не более чем $(n+1)$ частей в зависимости от того, на какой из отрезков $I^k_l$, траектория попадет после очередного шага.

Для вычисления энтропии остается указать, что из закона больших чисел следует, что для произвольно малого $\varepsilon$ с вероятностью, большей $1-\varepsilon$,
\begin{equation*}
  \frac{2}{\alpha+\beta}(1-\delta) < \frac{\nu}{n\Delta} < \frac{2}{\alpha+\beta}(1+\delta)
\end{equation*}
при любом $\delta$ и всех достаточно больших $n$. Поэтому
\begin{multline*}
  (1-\varepsilon)\frac{2}{\alpha+\beta}(1-\delta)
  \leq \frac{H(\prod_{i=0}^n T^{i\Delta}g^k)}{n\Delta} \leq\\
  \leq \varepsilon\cdot 2^k(2+\tfrac{\beta}{\alpha}) + \tfrac{2}{\alpha+\beta}(1+\delta) + \tfrac{\log(n+1)}{n\Delta} - \tfrac{\varepsilon \log\varepsilon + (1-\varepsilon)\log(1-\varepsilon)}{n\Delta}.
\end{multline*}

Устремляя $n$ к бесконечности, затем $\varepsilon$, $\delta$ к нулю, получил, что
\begin{equation*}
  h_{S^\Delta}(g^k)
  = \frac{2\Delta}{\alpha+\beta}.
\end{equation*}

Следует отметить, что равенство
\begin{equation*}
  h_{S^\Delta}(g^k)
  = \frac{2\Delta}{\alpha+\beta}
\end{equation*}
справедливо при любом $k$.
Если $\alpha$ и $\beta$ несоизмеримы, то разбиение $\{g^k\}$ является образующим при любом $k$, и мы получаем пример автоморфизма с конечным числом образующих, вкрадывающегося в поток.

Что касается спектра, то нетрудно показать, что в случае несоизмеримых $\alpha$ и $\beta$ спектр будет счетнократным лебеговским\footnote{Этот результат получен также Гирсановнм.}.
Таким образом, рассмотренные потоки доставляют нам примеры динамических систем, спектрально изоморфных, а метрически неизоморфных.

\section{\texorpdfstring{}{§ 3}}\label{2ndexample}

Рассмотрим пример потока, опубликованный в работе \cite{kolmogorov}.
Он представляет собой марковский процесс, у которого фазовым пространством является счетная совокупность отрезков $\Gamma_i = \{A_i, B_i\}$ длины $u/2^i$.
На каждом отрезке происходит детерминированное движение слева направо с единичной скоростью до правого конца отрезка, из которого с вероятностью $1/2^l$ совершается скачок в точку $A_l$.
Стационарное распределение следующее:
вероятность выбора $l$-го отрезка равна $3/4^l$, а на $l$-м отрезке распределение равномерное.

Разделим каждый отрезок на отрезки $I^k_l$ длины $u/2^k$, если такое деление возможно.
Остальные отрезки оставим без изменения.
Пусть $h^k$ есть разбиение на траектории, находящиеся в начальный момент времени в отрезках $I^k_l$.
Обозначим через $M_{i_1,i_2,\ldots i_l}$ совокупность траекторий, которые за время от --- $\Delta$ до $0$ находились полностью на отрезках $\Gamma_{i_1},\ldots, \Gamma_{i_l}$.
Для этого, очевидно, необходимо, чтобы $\tfrac{u}{2^{i_1}} + \tfrac{u}{2^{i_2}} + \ldots + \tfrac{u}{2^{i_l}} < \Delta$.
Пусть $M_0$ --- множество траекторий, которые за время от $-\Delta$ до $0$ не претерпели более одного скачка.
Обозначим через $m$ разбиение пространства на
множества $M_{i_1,i_2,\ldots i_l}$ и $M_0$.
Пусть $g^k = m\cdot h^k$.
Тогда последовательность разбиений $g^k$ удовлетворяет условиям теоремы \ref{intro}.

Разбиение множеств $G^k_i$ со временем происходит вполне аналогично рассмотренному \ref{1stexample}.
При фиксации порядка скачков мы получаем разбиение $G^k_l$ на множества, условная мера которых равна
\begin{equation*}
  p = 2^{-\nu_1-\nu_2 - \ldots -\nu_k},
\end{equation*}
где $\nu_1,\ldots,\nu_k$ и --- номера отрезков, на которых траектория оказывается после очередных скачков.
Из закона больших чисел следует, что с вероятностью $1-\varepsilon$ число скачков $k$ удовлетворяет неравенству
\begin{equation*}
  \tfrac{3}{u}(1-\delta)
  \leq \tfrac{k}{n\Delta}
  \leq \tfrac{3}{u} (1+\delta),
\end{equation*}
а вероятности соответствующих скачков неравенству
\begin{equation*}
 \tfrac{6}{u}(1-\delta_1)
 \leq -\tfrac{\log p}{n\Delta}
 \leq \tfrac{6}{u} (1+\delta_1).
\end{equation*}

Каждое множество с фиксированным порядком скачков разбивается еще на не более чем $k+n+1$ частей в зависимости от положения траекторий в моменты $i\Delta$.
Поэтому
\begin{multline*}
  (1-\varepsilon)\frac{6}{u}(1-\delta_1)
  \leq  \frac{H(\prod_{i=0}^n S^{i\Delta}g^k)}{n\Delta} \leq\\
  \leq \varepsilon f(k) + \tfrac{6}{u}(1+\delta_1) + \tfrac{\log n \left(1+ \Delta\tfrac{3}{u}(1+\delta)\right)}{n\Delta} - \tfrac{\varepsilon \log\varepsilon + (1-\varepsilon)\log(1-\varepsilon)}{n\Delta}.
\end{multline*}
где $f(k)$ --- энтропия начального распределения $g^k$.
Устремим $n$ к бесконечности‚ а $\varepsilon$, $\delta$, $\delta_1$ к нулю.
Получим, что $\tfrac{1}{\Delta} h_{S^\Delta}(g^k) = \tfrac{6}{u}$.
Следовательно, $h_S = 6/u$.

В заключение считаю своим приятным долгом выразить благодарность А.\ Н.\ Колмогорову за руководство при выполнении настоящей работы.

\phantomsection
\addcontentsline{toc}{section}{Список литературы}

\begin{thebibliography}{10}
\bibitem[1]{kolmogorov}{\sc А. Н. Колмогоров,}{}
  \newblock{ДАН, \textbf{119}, \textnumero\ 5 (1958).}
\bibitem[2]{rokhlin}{\sc В. А. Рохлин,}{}
  \newblock{Усп. матем. наук, \textbf{4}, 2 (30) (1949).}
\bibitem[3]{sinai}{\sc Я. Синай,}{}
  \newblock{ДАН, \textbf{124}, \textnumero\ 4 (1959).}
\end{thebibliography}
\end{document}
