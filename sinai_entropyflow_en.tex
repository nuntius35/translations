\documentclass[11pt, a4paper]{article}

\usepackage{textcomp}
\usepackage[a4paper]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage[final,
            activate={true,nocompatibility},
            babel,
            kerning=true,
            spacing=true,
            factor=1700,
            stretch=20,
            shrink=50]
            {microtype}
\usepackage[all]{nowidow}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{bookmark}
\usepackage{enumitem}

\renewcommand{\thesection}{\S~\arabic{section}.}
\newtheorem*{thm}{Theorem}

\hypersetup{
  pdfinfo={
      Title={On flows with finite entropy},
      Author={Ya. Sinai},
      Subject={Translated from the Russian original by Andreas Geyer-Schulz}
  }
}

\begin{document}

\author{Ya. Sinaĭ}
\title{On flows with finite entropy}
\date{}
\maketitle


\section{\texorpdfstring{}{§ 1}}\label{intro}

Consider the flow $(M, \mathfrak{S}, S^t)$, where $M$ is a Lebesgue space with measure $\mu $, $\mathfrak{S}$ is the $\sigma$-algebra of its measurable sets and $S^t$, $-\infty < t < \infty$, is a group of measure-preserving transformations acting on this space (see. \cite{rokhlin}).
For each automorphisms of this group, we can introduce the entropy $ h_{S^\Delta} = h_S (\Delta)$ according to \cite{sinai} .
In all known examples, the dependence of this function on $\Delta$ is linear, but the proof of this fact in the general case is absent.
Therefore, we call
\begin{equation*}
  \sup _{\Delta> 0} \frac{h_S (\Delta)}{\Delta}
  = h_S
\end{equation*}
the entropy of the flow $S^t$.

To calculate the entropy of a flow, it is convenient to use the following theorem, which is valid for an arbitrary automorphism $T$.
\begin{thm}
  Let $\{g_k\}$ be a sequence of partitions such that $g_k \leq g_{k + 1}$,
  \begin{equation*}
    \prod \limits_{k} \prod \limits_{n = - \infty}^\infty \{T^ng^k \}
    = \varepsilon,
  \end{equation*}
  where $\varepsilon$ is a divison into separate points, and $h(g_k) < \infty$.
  Then
  \begin{equation*}
    h_T
    = \lim_{k \to \infty} h_T(g_k).
  \end{equation*}
\end{thm}

The proof of this theorem is similar to the proof of Theorem~1 in \cite{sinai}.

In this note we show that there exist transitive flows with countable Lebesgue spectrum and for any finite $h_S > 0$.
Examples of flows with $h_S = \infty$ are trivial.
The example in \ref{2ndexample} has been published in Kolmogorov \cite{kolmogorov}.
The example in \ref{1stexample} is also due to Kolmogorov.

\section{\texorpdfstring{}{§ 2}}\label{1stexample}

Consider a Markov process, whose phase space are two intervals $A_1 B_1$ and $A_2 B_2$ with lengths $\alpha$ and $\beta$, respectively.
Inside each interval, there is a deterministic motion to the right with unit speed.
From the right end of each interval ($B_1$ or $B_2$) with probability $1/2$, $1/2$ there is a jump to the point $A_1$ or $A_2$.
It is known that the characteristics of the local motion assignment together with the specification of an initial distribution uniquely determines a measure on the trajectory space of the Markov process.
If we take the uniform distribution on both intervals as the initial distribution, then the resulting Markov process is stationary.

To calculate the entropy of the flow obtained above, we need to calculate the entropy corresponding to the automorphism $S^\Delta$.
We divide the interval $A_1 B_1$ into intervals of length $\alpha/2^k$.
We split the interval $A_2 B_2$ into intervals of the same length $\alpha/2^k$, except perhaps the last.
We obtain in total $2^k [1+ \tfrac{\beta}{\alpha} +1]$ intervals $I^k_l$.
Let $\{g_k\}$ be a partition of the whole space into sets $G^k_l$, formed by trajectories that are at time $0$ intervals $I^k_l$.
It is easy to see that for $\Delta < \min(\alpha, \beta)$, the sequence of partitions $g^k$ satisfies the conditions of the theorem.

Consider what happens with the sets $G^k_l$ when applying the transformation $S^{\Delta n}$ to them.
In other words, we need to consider how the sets $G^k_l$ are splitted depending on the position of the possible trajectories of the Markov process at time $0, \Delta, \ldots, n \Delta, \ldots$.
In the case of deterministic motion trajectories during the time from $0 $ to $\Delta $, the set $G^k_l$ is split in at most two sets.
In the case of sets $G^k_l$, consisting of a trajectory, making a jump during the time from $0$ to $\Delta$, the set $G^k_l$ decomposes into the sum of two sets $G^k_{l_1}$ and $G^k_{l_2}$ with conditional measure $1/2$, depending on which of the intervals $ A_1B_1$ or $A_2B_2$ the trajectory would be at time $\Delta$.
Further study of the decomposition can be carried out for each of the sets $G^k_{l_1}$ and $G^k_{l_2}$ independently.
Some of the set $G^k_l$ may be such that one part of the trajectories must make a jump in the time from $0$ to $\Delta$, and the other part is moving deterministically.
The partition in this case is a combination of the previous two cases and does not differ from them.

For $n$ steps the set $G^k_l$ splits into separate subsets depending on the number of jumps in the  trajectories.
The conditional measure of each such subset is equal to $2^{- \nu}$, where $\nu$ is the number of jumps.
Moreover, each subset having a fixed number of jumps will be split into no more than  $(n + 1)$ parts depending on which of the intervals $I^k_l$ the trajectory reaches after the next step.

To calculate the entropy, it remains to point out that the law of large numbers implies that for arbitrarily small $\varepsilon$ we have
\begin{equation*}
  \frac{2}{\alpha + \beta} (1- \delta) <\frac{\nu}{n \Delta} <\frac{2}{\alpha + \beta} (1+ \delta)
\end{equation*}
with probability greater than $1 - \varepsilon$ for any $\delta $ and all sufficiently large $n$.
Therefore,
\begin{multline*}
  (1- \varepsilon) \frac{2}{\alpha + \beta} (1- \delta)
  \leq \frac{H (\prod_{i = 0}^n T^{i \Delta} g^k) }{n \Delta} \leq \\
  \leq \varepsilon \cdot 2^k (2+ \tfrac{\beta}{\alpha}) + \tfrac{2}{\alpha + \beta} (1+ \delta) + \tfrac{\log (n + 1 )}{n \Delta} - \tfrac{\varepsilon \log \varepsilon + (1- \varepsilon) \log (1- \varepsilon)}{n \Delta}.
\end{multline*}

Letting $n$ tend to infinity, we find that $\varepsilon$, $\delta$ tend to zero and that
\begin{equation*}
  h_{S^\Delta} (g^k)
  = \frac{2 \Delta}{\alpha + \beta}.
\end{equation*}

It should be noted that the equality
\begin{equation*}
  h_{S^\Delta} (g^k)
  = \frac{2 \Delta}{\alpha + \beta}
\end{equation*}
is valid for any $k$.
If $\alpha$ and $\beta$ are incommensurable, then the partition $\{g^k\}$ is a generator for any $k$, and we have an example of an automorphism with a finite number of generators embedded in a flow.

With regard to the spectrum, it is easy to show that in the case of incommensurable $\alpha$ and $\beta$ the Lebesgue spectrum is countable\footnote{This result was obtained by Girsanov.}.
Thus, the considered flows give us examples of dynamical systems, which are spectrally isomorphic but metrically nonisomorphic.

\section{\texorpdfstring{}{§ 3}}\label{2ndexample}

Consider the example of a flow, published in \cite{kolmogorov}.
It is a Markov process, whose phase space is a countable set of intervals $\Gamma_i = \{A_i, B_i\}$ of length $u/2^i$.
On each interval there is a deterministic motion to the right with unit speed, from which, with probability $1/2^l$ there is a jump to the point $A_l$.
The stationary distribution is the following:
the probability of choosing the $l$-th interval is equal to $3/4^l$, and the distribution is uniform on the $l$-th interval .

Divide each interval into intervals $I^k_l$ of length $u/2^k$, if such a division is possible.
The remaining intervals are unaffected.
Let $h^k$ be a partition into trajectories that are at the initial time intervals $I^k_l$.
We denote by $M_{i_1, i_2, \ldots i_l}$ the set of trajectories which remain from time $\Delta$ to $0$ entirely in the intervals $\Gamma_{i_1}, \ldots, \Gamma_{i_l}$.
For this purpose, it is obviously necessary that $\tfrac{u}{2^{i_1}} + \tfrac{u}{2^{i_2}} + \ldots + \tfrac{u}{2^{i_l}} < \Delta $.
Let $M_0$ be the set of trajectories that during the time from $-\Delta $ to $0$ have not undergone more than one jump.
Denote by $m$ a partition of the space into $M_{i_1, i_2, \ldots i_l}$ and $M_0$.
Let $g^k = m \cdot h^k$.
Then the sequence of partitions $g^k$ satisfies the conditions of the theorem of \ref{intro}.

The splitting of the set $G^k_i$ eventually happens quite similar to that considered in \ref{1stexample}.
When fixing the order of the jumps, we obtain a partition $G^k_l$ into sets which have the conditional measure
\begin{equation*}
  p
  = 2^{- \nu_1- \nu_2 - \ldots -\nu_k},
\end{equation*}
where $\nu_1, \ldots, \nu_k$ are the number of intervals in which the trajectory is after next jump.
The law of large numbers implies that with probability
$1 - \varepsilon$ the number of jumps $k$ satisfies the inequality
\begin{equation*}
  \tfrac{3}{u} (1 - \delta)
  \leq \tfrac{k}{n \Delta}
  \leq \tfrac{3}{u} (1 + \delta),
\end{equation*}
and the probabilities of the corresponding jumps satisfy
\begin{equation*}
 \tfrac{6}{u} (1- \delta_1)
 \leq - \tfrac{\log p}{n \Delta}
 \leq \tfrac{6}{u} (1+ \delta_1).
\end{equation*}

Each set with a fixed order of the jumps is broken
into no more than $k + n + 1$ parts depending on the position of the trajectory at the time $i \Delta$.
Therefore
\begin{multline*}
  (1- \varepsilon) \frac{6}{u} (1- \delta_1)
  \leq \frac{H (\prod_{i = 0}^n S^{i \Delta} g^k)}{n \Delta} \leq \\
  \leq \varepsilon f (k) + \tfrac{6}{u} (1+ \delta_1) + \tfrac{\log n \left (1+ \Delta \tfrac{3}{u} (1+ \delta ) \right)}{n \Delta} - \tfrac{\varepsilon \log \varepsilon + (1 - \varepsilon) \log (1- \varepsilon)}{n \Delta},
\end{multline*}
where $f(k)$ is entropy of the initial distribution of $g^k$.
We let $n$ tend to infinity, and $\varepsilon$, $\delta$ and $\delta_1$ to zero.
We find that $\tfrac{1}{\Delta} h_{S^\Delta} (g^k) = \tfrac{6}{u}$.
Therefore, $h_S = 6/u$.

In conclusion, it is my pleasant duty to thank Kolmogorov for guiding me in carrying out this work.

\phantomsection
\addcontentsline{toc}{section}{References}

\begin{thebibliography}{10}
\bibitem[1]{kolmogorov}{\sc Kolmogorov,}{}
  \newblock{DAN, \textbf{119}, \textnumero\ 5 (1958).}
\bibitem[2]{rokhlin}{\sc Rokhlin,}{}
  \newblock{Phys. Mat. Sciences, \textbf{4}, 2 (30) (1949).}
\bibitem[3]{sinai}{\sc Ya. Sinai,}{}
  \newblock{DAN, \textbf{124}, \textnumero\ 4 (1959).}
\end{thebibliography}
\end{document}
